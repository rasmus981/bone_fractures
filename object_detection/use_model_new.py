import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile

from distutils.version import StrictVersion
from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image
import cv2
from keras.models import load_model
import matplotlib.patches as patches
from matplotlib.patches import Rectangle

# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")
from object_detection.utils import ops as utils_ops

if StrictVersion(tf.__version__) < StrictVersion('1.9.0'):
	raise ImportError('Please upgrade your TensorFlow installation to v1.9.* or later!')


from utils import label_map_util

from utils import visualization_utils_new as vis_util


# What model to download.
MODEL_NAME = 'final_graph'

# Path to frozen detection graph. This is the actual model that is used for the object detection.
PATH_TO_FROZEN_GRAPH = MODEL_NAME + '/frozen_inference_graph.pb'

# List of the strings that is used to add correct label for each box.
PATH_TO_LABELS = os.path.join('training', 'object-detection.pbtxt')

# NUM_CLASSES = 2


detection_graph = tf.Graph()
with detection_graph.as_default():
	od_graph_def = tf.GraphDef()
	with tf.gfile.GFile(PATH_TO_FROZEN_GRAPH, 'rb') as fid:
		serialized_graph = fid.read()
		od_graph_def.ParseFromString(serialized_graph)
		tf.import_graph_def(od_graph_def, name='')

category_index = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS, use_display_name=True)



def load_image_into_numpy_array(image):
	(im_width, im_height) = image.size
	return np.array(image.getdata()).reshape((im_height, im_width, 3)).astype(np.uint8)


# For the sake of simplicity we will use only 2 images:
# image1.jpg
# image2.jpg
# If you want to test the code with your images, just add path to the images to the TEST_IMAGE_PATHS.
PATH_TO_TEST_IMAGES_DIR = 'images/val'
TEST_IMAGE_PATHS = [ os.path.join(PATH_TO_TEST_IMAGES_DIR, 'image{}.png'.format(i)) for i in range(1,15) ]

# Size, in inches, of the output images.
IMAGE_SIZE = (12, 8)

def rgb2gray(rgb):
    return np.dot(rgb[...,:3], [0.299, 0.587, 0.114])


def run_inference_for_single_image(image, graph):
	with graph.as_default():
		with tf.Session() as sess:
			# Get handles to input and output tensors
			ops = tf.get_default_graph().get_operations()
			all_tensor_names = {output.name for op in ops for output in op.outputs}
			tensor_dict = {}
			for key in [
				'num_detections', 'detection_boxes', 'detection_scores',
				'detection_classes', 'detection_masks'
			]:
				tensor_name = key + ':0'
				if tensor_name in all_tensor_names:
					tensor_dict[key] = tf.get_default_graph().get_tensor_by_name(tensor_name)
			if 'detection_masks' in tensor_dict:
				# The following processing is only for single image
				detection_boxes = tf.squeeze(tensor_dict['detection_boxes'], [0])
				detection_masks = tf.squeeze(tensor_dict['detection_masks'], [0])
				# Reframe is required to translate mask from box coordinates to image coordinates and fit the image size.
				real_num_detection = tf.cast(tensor_dict['num_detections'][0], tf.int32)
				detection_boxes = tf.slice(detection_boxes, [0, 0], [real_num_detection, -1])
				detection_masks = tf.slice(detection_masks, [0, 0, 0], [real_num_detection, -1, -1])
				detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(					detection_masks, detection_boxes, image.shape[0], image.shape[1])
				detection_masks_reframed = tf.cast(
					tf.greater(detection_masks_reframed, 0.5), tf.uint8)
				# Follow the convention by adding back the batch dimension
				tensor_dict['detection_masks'] = tf.expand_dims(
					detection_masks_reframed, 0)
			image_tensor = tf.get_default_graph().get_tensor_by_name('image_tensor:0')

			# Run inference
			output_dict = sess.run(tensor_dict,
					feed_dict={image_tensor: np.expand_dims(image, 0)})

			# all outputs are float32 numpy arrays, so convert types as appropriate
			output_dict['num_detections'] = int(output_dict['num_detections'][0])
			output_dict['detection_classes'] = output_dict[
				'detection_classes'][0].astype(np.uint8)
			output_dict['detection_boxes'] = output_dict['detection_boxes'][0]
			output_dict['detection_scores'] = output_dict['detection_scores'][0]
			if 'detection_masks' in output_dict:
				output_dict['detection_masks'] = output_dict['detection_masks'][0]
	return output_dict

model = load_model('my_model_test.h5')
for image_path in TEST_IMAGE_PATHS:
	image = Image.open(image_path)
	#plt.imshow(image)
	if image.format == "PNG":
		image = image.convert('RGB')
	# the array based representation of the image will be used later in order to prepare the
	# result image with boxes and labels on it.
	image_np = load_image_into_numpy_array(image)
	image_no_boxes = load_image_into_numpy_array(image)
	# Expand dimensions since the model expects images to have shape: [1, None, None, 3]
	image_np_expanded = np.expand_dims(image_np, axis=0)
	# Actual detection.
	output_dict = run_inference_for_single_image(image_np, detection_graph)
	# Visualization of the results of a detection.
	_, boxes = vis_util.visualize_boxes_and_labels_on_image_array(
		image_np,
		output_dict['detection_boxes'],
		output_dict['detection_classes'],
		output_dict['detection_scores'],
		category_index,
		instance_masks=output_dict.get('detection_masks'),
		use_normalized_coordinates=True,
		line_thickness=2)
	# image_np = rgb2gray(image_np)
	# plt.figure(figsize=IMAGE_SIZE)
	# plt.imshow(image_np)
	# plt.show()
	#print(boxes)
	img = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE) / 255.0

	chicken_legs = []
	chicken_legs_position = []
	chicken_class = []
	for (box, color) in boxes:
	   	(left, right, top, bottom) = box
	   	chicken_legs_position.append((int(left), int(right), int(top), int(bottom)))
	   	if color == 'Chartreuse':
	   		chicken_class.append(0)
	   	else:
	   		chicken_class.append(1)
	   	chicken_legs.append(img[int(top):int(bottom), int(left):int(right)])


	# Create figure and axes
	fig,ax = plt.subplots(1)

	# Display the image
	ax.imshow(img)
	rects = []
	for i in range(len(chicken_legs)):
		if chicken_class[i] == 1:
			leg = chicken_legs[i]
			rotated = 0
			if leg.shape[0] < leg.shape[1]:
				leg = np.rot90(leg, 1)
				rotated = 1
			height = leg.shape[0]
			split_size = height // 3
			splits = 3
			overlap = 5
			pos = chicken_legs_position[i]
			
			
			for j in range(splits):
				if j == 0:
					image_split_0 = leg[:(j + 1) * split_size + overlap]
					if rotated == 1:
						image_split_0_pos = (pos[1] - split_size + overlap , pos[1], pos[2], pos[3])
					else:
						image_split_0_pos = (pos[0], pos[1], pos[2], pos[2] + split_size + overlap)
				elif j + 1 == splits:
					image_split_2 = leg[j * split_size - overlap :]
					if rotated == 1:
						image_split_2_pos = (pos[0] , pos[0] + split_size + overlap, pos[2], pos[3])
					else:
						image_split_2_pos = (pos[0], pos[1], pos[3] - split_size - overlap, pos[3])
				else:
					image_split_1 = leg[j * split_size - overlap : (j + 1) * split_size + overlap]
					if rotated == 1:
						image_split_1_pos = (pos[0] + split_size - overlap , pos[1] - split_size + overlap, pos[2], pos[3])
					else:
						image_split_1_pos = (pos[0], pos[1], pos[2] + split_size - overlap, pos[3] - split_size + overlap)


			image_split_0_resize = cv2.resize(image_split_0, (150, 50))
			image_split_1_resize = cv2.resize(image_split_1, (150, 50))
			image_split_2_resize = cv2.resize(image_split_2, (150, 50))
			# split_0_guess = np.argmax(model.predict(image_split_0_resize.reshape(1, 50, 150, 1)))
			# split_1_guess = np.argmax(model.predict(image_split_1_resize.reshape(1, 50, 150, 1)))
			# split_2_guess = np.argmax(model.predict(image_split_2_resize.reshape(1, 50, 150, 1)))

			# if split_0_guess == 1:
			# 	rect = patches.Rectangle((image_split_0_pos[0],image_split_0_pos[2]),(image_split_0_pos[1] - image_split_0_pos[0]),(image_split_0_pos[3] - image_split_0_pos[2]),linewidth=1,edgecolor='r',facecolor='none')
			# 	rects.append(rect)
			# if split_1_guess == 1:
			# 	rect = patches.Rectangle((image_split_1_pos[0],image_split_1_pos[2]),(image_split_1_pos[1] - image_split_1_pos[0]),(image_split_1_pos[3] - image_split_1_pos[2]),linewidth=1,edgecolor='r',facecolor='none')
			# 	rects.append(rect)
			# if split_2_guess == 1:
			# 	rect = patches.Rectangle((image_split_2_pos[0],image_split_2_pos[2]),(image_split_2_pos[1] - image_split_2_pos[0]),(image_split_2_pos[3] - image_split_2_pos[2]),linewidth=1,edgecolor='r',facecolor='none')
			# 	rects.append(rect)

			split_0_guess = model.predict(image_split_0_resize.reshape(1, 50, 150, 1))[0][1]
			split_1_guess = model.predict(image_split_1_resize.reshape(1, 50, 150, 1))[0][1]
			split_2_guess = model.predict(image_split_2_resize.reshape(1, 50, 150, 1))[0][1]
			region_list = [split_0_guess, split_1_guess, split_2_guess]
			region = region_list.index(max(region_list))
			if region == 0:
				print("0")
				rect = patches.Rectangle((image_split_0_pos[0],image_split_0_pos[2]),(image_split_0_pos[1] - image_split_0_pos[0]),(image_split_0_pos[3] - image_split_0_pos[2]),linewidth=1,edgecolor='r',facecolor='none')
				rects.append(rect)
			elif region == 1:
				print("1")
				rect = patches.Rectangle((image_split_1_pos[0],image_split_1_pos[2]),(image_split_1_pos[1] - image_split_1_pos[0]),(image_split_1_pos[3] - image_split_1_pos[2]),linewidth=1,edgecolor='r',facecolor='none')
				rects.append(rect)
			elif region == 2:
				print("2")
				rect = patches.Rectangle((image_split_2_pos[0],image_split_2_pos[2]),(image_split_2_pos[1] - image_split_2_pos[0]),(image_split_2_pos[3] - image_split_2_pos[2]),linewidth=1,edgecolor='r',facecolor='none')
				rects.append(rect)

	for rect in rects:
		ax.add_patch(rect)
	plt.show()

