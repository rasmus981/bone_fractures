import os
import xml.etree.ElementTree as ET
from PIL import Image
import numpy as np
from matplotlib import pyplot as plt
import scipy.misc

def load_image_into_numpy_array(image):
	(im_width, im_height) = image.size
	return np.array(image.getdata()).reshape((im_height, im_width, 3)).astype(np.uint8)


#filename = "kylling-40kv-0.15ma-0.20tint-1000lr-04_12_2018-13_21_30_cut0.jpg"
folder = "images/test_chicken/"
new_folder = "images/split_test_chicken/"

def split_image(new_folder, folder, filename, splits, overlap):
	image = Image.open(folder + filename)
	image_np = load_image_into_numpy_array(image)
	if image_np.shape[0] < image_np.shape[1]:
		image_np = np.rot90(image_np, 1)

	height = image_np.shape[0]
	split_size = height // splits
	counter = 0
	for i in range(splits):
		if i == 0:
			image_split = image_np[:(i + 1) * split_size + overlap]
		elif i + 1 == splits:
			image_split = image_np[i * split_size - overlap :]
		else:
			image_split = image_np[i * split_size - overlap : (i + 1) * split_size + overlap]

		file_name = new_folder + filename[:-4] + "_" + str(counter) + ".jpg"
		scipy.misc.imsave(file_name, image_split)
		counter += 1

for filename in os.listdir(folder):
	if filename != ".DS_Store":
		split_image(new_folder, folder, filename, 3, 5)