import os
import xml.etree.ElementTree as ET
from PIL import Image
import numpy as np
from matplotlib import pyplot as plt
import scipy.misc


#xml_filename = "images/train/kylling-40kv-0.15ma-0.20tint-1000lr-22_11_2018-13_16_22_cut.xml"
#image_file = "kylling-40kv-0.15ma-0.20tint-1000lr-22_11_2018-13_16_22_cut.png"
#image = "images/train/" + image_file
#tree = ET.parse(xml_filename)
#root = tree.getroot()

def load_image_into_numpy_array(image):
	(im_width, im_height) = image.size
	return np.array(image.getdata()).reshape((im_height, im_width, 3)).astype(np.uint8)

def get_broken_element_positions(xml_filename):
	tree = ET.parse(xml_filename)
	root = tree.getroot()
	list_of_broken_elements = []
	for child in root:
		if child.tag == "object":
			if child.find('name').text == "broken":
				bndbox = child.find("bndbox")
				xmin = bndbox.find("xmin").text
				ymin = bndbox.find("ymin").text
				xmax = bndbox.find("xmax").text
				ymax = bndbox.find("ymax").text
				list_of_broken_elements.append((int(xmin), int(ymin), int(xmax), int(ymax)))
	return list_of_broken_elements


def get_broken_element_in_image(folder, new_folder, xml_filename, image_name):
	list_of_broken_elements = get_broken_element_positions(folder + xml_filename)
	image = Image.open(folder + image_name)
	if image.format == "PNG":
		image = image.convert('RGB')
	image_np = load_image_into_numpy_array(image)
	print(image_np.shape)
	counter = 0
	for (xmin, ymin, xmax, ymax) in list_of_broken_elements:
		test = image_np[ymin:ymax, xmin:xmax, :]
		file_name = new_folder + image_name[:-4] + str(counter) + ".jpg"
		scipy.misc.imsave(file_name, test)
		counter += 1

for filename in os.listdir('images/test'):
	if filename[-4:] == ".xml":
		xml_filename = filename
		image_name = filename[:-4] + ".png"
		folder = "images/test/"
		get_broken_element_in_image(folder, "images/test_chicken/", xml_filename, image_name)