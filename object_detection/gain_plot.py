# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

# Helper libraries
import numpy as np
import os
import xml.etree.ElementTree as ET
from PIL import Image
import numpy as np
import cv2
from matplotlib import pyplot as plt
import scipy.misc
from keras.models import Sequential
from keras.layers import Dense, Conv2D, Dropout, Flatten, MaxPooling2D
from keras.models import model_from_json


train_broken_folder = "test.png"


def load_image_into_numpy_array():
	img = cv2.imread("test.png", cv2.IMREAD_GRAYSCALE) / 255.0
	plt.imshow(img)
	plt.show()
	resize_img = cv2.resize(img, (150, 50))
	plt.imshow(resize_img)
	plt.show()

load_image_into_numpy_array()