# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

# Helper libraries
import numpy as np
import os
import xml.etree.ElementTree as ET
from PIL import Image
import numpy as np
import cv2
from matplotlib import pyplot as plt
import scipy.misc
from keras.models import Sequential
from keras.layers import Dense, Conv2D, Dropout, Flatten, MaxPooling2D
from keras.models import model_from_json


train_broken_folder = "images/split_train_broken/"
train_notbroken_folder = "images/split_train_notbroken/"
test_broken_folder = "images/split_test_broken/"
test_notbroken_folder = "images/split_test_notbroken/"

train_data_broken = []
train_data_notbroken = []
test_data_broken = []
test_data_notbroken = []


def load_image_into_numpy_array(image):
	(im_width, im_height) = image.size
	return np.array(image.getdata()).reshape((im_height, im_width, 3)).astype(np.uint8)


for filename in os.listdir(train_broken_folder):
	if filename != ".DS_Store":
		img = cv2.imread(train_broken_folder + filename, cv2.IMREAD_GRAYSCALE) / 255.0
		resize_img = cv2.resize(img, (150, 50))
		train_data_broken.append(resize_img)

for filename in os.listdir(train_notbroken_folder):
	if filename != ".DS_Store":
		img = cv2.imread(train_notbroken_folder + filename, cv2.IMREAD_GRAYSCALE) / 255.0
		resize_img = cv2.resize(img, (150, 50))
		train_data_notbroken.append(resize_img)

for filename in os.listdir(test_broken_folder):
	if filename != ".DS_Store":
		img = cv2.imread(test_broken_folder + filename, cv2.IMREAD_GRAYSCALE) / 255.0
		resize_img = cv2.resize(img, (150, 50))
		test_data_broken.append(resize_img)

for filename in os.listdir(test_notbroken_folder):
	if filename != ".DS_Store":
		img = cv2.imread(test_notbroken_folder + filename, cv2.IMREAD_GRAYSCALE) / 255.0
		resize_img = cv2.resize(img, (150, 50))
		test_data_notbroken.append(resize_img)

#print(test_data_broken[2][0][0])
np_train_data_broken = np.array(train_data_broken)
np_train_data_nobroken = np.array(train_data_notbroken)
np_test_data_broken = np.array(test_data_broken)
np_test_data_nobroken = np.array(test_data_notbroken)

#print(np_test_data_broken[2][0][0])

train_data_broken_labels = np.ones(len(np_train_data_broken))
train_data_notbroken_labels = np.zeros(len(np_train_data_nobroken))
test_data_broken_labels = np.ones(len(np_test_data_broken))
test_data_notbroken_labels = np.zeros(len(np_test_data_nobroken))

train_data =  np.concatenate((np_train_data_broken, np_test_data_broken, np_train_data_nobroken, np_test_data_nobroken), axis=0) 
train_labels =  np.concatenate((train_data_broken_labels, test_data_broken_labels, train_data_notbroken_labels, test_data_notbroken_labels), axis=0) 
test_data = np.concatenate((np_test_data_broken, np_test_data_nobroken), axis=0)
test_labels =  np.concatenate((test_data_broken_labels, test_data_notbroken_labels), axis=0) 

train_data = train_data.reshape(train_data.shape[0], 50, 150, 1)
test_data = test_data.reshape(test_data.shape[0], 50, 150, 1)


model = Sequential()
#add model layers
model.add(Conv2D(32, kernel_size=3, padding='same', strides=2, activation='relu', input_shape=(50,150,1)))
model.add(Conv2D(32, kernel_size=3, padding='same', strides=2, activation='relu'))
model.add(Flatten())
model.add(Dense(1024, activation='relu'))
model.add(Dense(512, activation='relu'))
model.add(Dense(2, activation='softmax'))

model.compile(optimizer='adam', 
              loss='sparse_categorical_crossentropy', 
              metrics=['accuracy'])
model.summary()
model.fit(x=train_data,y=train_labels, epochs=50)
print(model.evaluate(test_data, test_labels))

counter_0 = 0
counter_1 = 0

for i in range(len(test_labels)):
	a = model.predict(test_data[i].reshape(1, 50, 150, 1))
	guess = np.argmax(a)
	if guess == 0:
		counter_0 += 1
	else:
		counter_1 += 1

print(counter_0)
print(counter_1)

# Model reconstruction from JSON file
model.save('my_model_test.h5')



