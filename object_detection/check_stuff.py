# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

# Helper libraries
import numpy as np
import os
import xml.etree.ElementTree as ET
from PIL import Image
import numpy as np
import cv2
from matplotlib import pyplot as plt
import scipy.misc
from keras.models import Sequential
from keras.layers import Dense, Conv2D, Dropout, Flatten, MaxPooling2D


img1 = cv2.imread("images/split_test_notbroken/kylling_test_broken_yes-40kv-0.15ma-0.20tint-1000lr-06_12_2018-13_45_12_cut0_0.jpg", cv2.IMREAD_GRAYSCALE)
