# Detection of bone fractures in chicken legs

Project by Rasmus Skov Johannesson

## Data folder

Contains all data and a python file named image_libary.py to preprocces the data.

### Object detection foler

First download the tensorflow reasearch API (https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/installation.md), then change the premade object_detection folder with this one. 

In the folder you will see a number of python files,

* get_chicken_image.py (used to get full chiken legs out of an image)
* split_chiken_image.py (used to split the chicken images)
* use_model_new.py (uses the faster-rcnn and the RFD)
* use_model.py (uses faster-rcnn)
* my_model.h5 (pre trained weights for RFD network)