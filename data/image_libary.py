import matplotlib.pyplot as plt
import pickle
import sys
import numpy
import warnings
import scipy.misc
import scipy.ndimage
from skimage import feature
from images import test_images#, useless_images
import scipy.misc
import png


def image_from_pickle(filename):
	return pickle.load(open(filename, "rb"), encoding="latin1")

def plot_image(image):
	plt.imshow(image)
	plt.show()

def plot_image_extent_axis(image):
	plt.imshow(image, extent=[0, image.shape[1], 0, image.shape[0]])
	plt.show()

def trols_calibration(image):
	with open("black.pickle", "rb") as f:
		black = pickle.load(f, encoding="latin1")
	
	# Convert for easier calculations (raw image is in int16)
	image = image.astype(numpy.float64)

	# Sample max energy level (requires X )
	white = image[:80]
	white = numpy.mean(white, axis=0)
	print(image.shape)
	image = troels_interpolate(image, 3.0, mode="spline")
	print(image.shape)
	# Linear Calibration
	image = (image - black) / (white - black)
	print(image.shape)
	return image


def troels_interpolate(image, ratio, mode):
    if mode == "lanczos":
        new_image = scipy.misc.imresize(image, (int(image.shape[0] / ratio), image.shape[1]),
                                        interp="lanczos")
    elif mode == "spline":
        with warnings.catch_warnings():
            # Suppress unimportant warning from scipy.ndimage.zoom
            warnings.simplefilter("ignore")
            return(scipy.ndimage.zoom(image, (1. / ratio, 1.), order=5))  # 5 is the highest
    else:
        raise Exception("Modes supported: 'lanczos' and 'spline'")


def edges_image(image, sigma=1.0):
	return(feature.canny(image, sigma))


def find_min_index(image):
	for i in range(len(image)):
		if True in image[i]:
			return i

def find_max_index(image):
	for i in reversed(range(len(image))):
		if True in image[i]:
			return i

def find_min_max_index(image):
	min_index = find_min_index(image)
	max_index = find_max_index(image)
	return min_index, max_index


def save_png(filename, image):
	plt.imsave(filename, image)


# for filename in useless_images:
# 	image = image_from_pickle(filename)
# 	image = trols_calibration(image)
# 	plot_image(image)



for filename in test_images:
	print(filename)
	image = image_from_pickle(filename)
	plot_image(image)
	image = trols_calibration(image)
	plot_image(image)
	image_edge = edges_image(image, sigma=5)
	plot_image(image_edge)
	min_index, max_index = find_min_max_index(image_edge)
	image = image[min_index:max_index]
	#filename = filename[:-7] + "_cut.png"
	#save_png(filename, image)
	plot_image(image)
	# file = open(filename, 'wb')
	# pickle.dump(image, file)
	# file.close()

# print("done")


# for filename in test_images_cut:
#  	image = image_from_pickle(filename)
#  	plot_image_extent_axis(image)


